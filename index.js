// Setting prototypes - tmp.gpro
/**
 * Find a string in the array matching given regex
 * @param {String} match String of regex to match with string
 * @return {String|null} Returns matching string or null if nothing is found
 */
Array.prototype.findReg = function(match) {
	var reg = new RegExp(match)

	return this.filter(function(item){
		return typeof item == 'string' && item.match(reg)
	})
}

/**
 * Removed value from array
 * @param {*} value Value to look for and remove out of array
 * @return {Boolean} Returns true on success (removed value or value not found) and false if the given value is empty
 * @description Removed value from array. Note that this function doesn't stop at the first match!
 */
Array.prototype.removeByValue = function(value) {
	if (isEmpty(value)) return false
	for (let i = 0; i < this.length; i++) {
		const element = this[i];
		if (element == value) this.splice(i, 1)
	}
	return true
}

Object.prototype.size = function() {
	return Object.keys(this).length
}

// Name of the script
const programName = "Operation Center"
// Setting module name which will be used by debug handler
const moduleName = "OC"

// Global variables
const versionNumber = "v0.0.56"
const versionType = "ALPHA"
const dataSaveLocation = "./data.bin"

// Initialize debug and error handling
const { DroidsError } = require("droid-errors")

// Error Classes - tmp.derrors
class MissingFile extends DroidsError {
	/**
	* Constructing MissingFile
	* @param {String} errMsg **The error message*
	* @param {String} moduleName **Custom module name*
	* @param {Error} cause **If re-throwing pass the original cause here*
	*/
	constructor(fileLoc, moduleName, cause) {
		super(`File '${fileLoc}' couldn't be found!`, moduleName, cause)
	}
}

class MissingArgument extends DroidsError {
	/**
	* Constructing SecurityError
	* @param {String} errMsg **The error message*
	* @param {String} moduleName **Custom module name*
	* @param {Error} cause **If re-throwing pass the original cause here*
	*/
	constructor(missingVarName, moduleName, cause) {
		super("Missing argument '" + missingVarName + "'", moduleName, cause)
	}
}

class Exists extends DroidsError {
	/**
	* Constructing Exists
	* @param {String} errMsg **The error message*
	* @param {String} moduleName **Custom module name*
	* @param {Error} cause **If re-throwing pass the original cause here*
	*/
	constructor(element = "element", moduleName, cause) {
		super(`This ${element} already exists.`, moduleName, cause)
	}
}

class IncorrectArgument extends DroidsError {
	/**
	* Constructing SecurityError
	* @param {String} errMsg **The error message*
	* @param {String} moduleName **Custom module name*
	* @param {Error} cause **If re-throwing pass the original cause here*
	*/
	constructor(incorrectVarName, descriptionRightUse, moduleName, cause) {
		super(`Argument '${incorrectVarName}' is used incorrectly. Usage:\n${descriptionRightUse}`, moduleName, cause)
	}
}

class NotExist extends DroidsError {
	/**
	* Constructing NotExist
	* @param {String} errMsg **The error message*
	* @param {String} moduleName **Custom module name*
	* @param {Error} cause **If re-throwing pass the original cause here*
	*/
	constructor(moduleName, errMsg = "Whatever you are requesting couldn't be found.", cause) {
		super(errMsg, moduleName, cause)
	}
}

class ImpossibleState extends DroidsError {
	/**
	* Constructing ImpossibleState
	* @param {String} moduleName **Custom module name*
	* @param {Error} cause **If re-throwing pass the original cause here*
	* @param {String} errMsg **The error message*
	*/
	constructor(moduleName, cause, errMsg) {
		super("NOBODY EXPECTS THE SPANISH INQUISITION!\n" + errMsg, moduleName, cause)
	}
}

// Setting Debug
const DroidsDebug = require("droid-debug")
// Enabling debug mode
var debugHand = new DroidsDebug(0)
if (process.argv.indexOf("debug") != -1) {
	debugHand = new DroidsDebug(1)
} else if (process.argv.indexOf("killme") != -1) {
	debugHand = new DroidsDebug(2)
} else if (process.argv.indexOf("devops") != -1) {
	debugHand = new DroidsDebug(3)
}

// Flags - tmp.flags


// Loading modules - tmp.modules
// Load express module
const express = require("express")
debugHand.log("Module 'express' loaded as 'express'.", 1, moduleName)
// Load fs module
const fs = require("fs")
debugHand.log("Module 'fs' loaded as 'fs'.", 1, moduleName)
// Load async module
const async = require("async")
debugHand.log("Module 'async' loaded as 'async'.", 1, moduleName)
// Load path module
const path = require("path")
debugHand.log("Module 'path' loaded as 'path'.", 1, moduleName)

// Prototypes of loaded modules - tmp.proto.modules
/**
* @param {String} path The path to a file you want to 'touch'
* @returns {Promise} Returns a Promise. On resolve it returns true if the file got touched, reject returns an error if any
* @requires fs
* @desc Basiclly the Linux `touch` command made with `fs`
*/
touch = function(path) {
	return new Promise(function(resolve, reject) {
		fs.open(path, 'a', function(err, fnd) {
			if (err) reject(err)
			fs.close(fnd, function(err2) {
				if (err2) reject(err2)
				resolve(true)
			})
		})
	})
}

/**
* @abstract
* @param {!String} path The path to a file you want to write the `data` to
* @param {*} data The data you want to write to the file
* @param {?Object} [options={}] An object with `fs.writeFile()` options
* @returns {!Promise} Returns a Promise. On resolve it returns true if the data got written succesfully, reject returns a error if any
* @see fs.writeFile()
* @see fs.writeFileSync()
* @requires fs
* @desc Same as `fs.writeFileSync()` but with error handling support
*/
writeFileSync = function(path, data, options = {}) {
	return new Promise(function(resolve, reject) {
		fs.writeFile(path, data, options, function(err) {
			if (err) reject(err)
			resolve(true)
		})
	})
}

/**
* @abstract
* @param {!String} path The path to a file you want to read
* @param {?Object} [options={}] An object with `fs.readFile()` options
* @returns {!Promise>} Returns a Promise. On resolve it returns the read data, reject returns a error if any
* @see fs.readFile()
* @see fs.readFileSync()
* @requires fs
* @desc Same as `fs.readFileSync()` but with error handling support
*/
readFileSync = function(path, options) {
	return new Promise(function(resolve, reject) {
		fs.readFile(path, options, function(err, dataBuffer) {
			if (err) reject(err)
			resolve(dataBuffer.toString("utf8"))
		})
	})
}

// Classes
/*
 {
 	 <department>: {
		 employers: [
			{
				rp_name: "",
				rang: "",
				steam_id: "",
				main_callsign: <callsign but integer>, (disable, is automaticlly calculated from first callsign)
				callsigns: {
					default: <default role>
					<role_name>: {
						callsign: <callsign>
						identity: <name of identity> (optional, defaults to null)
					} // Next role callsign (First one required, after that optional)
				},
				current_role: "", (disable, defaults to 'Agent')
				specials: [<specialties>], (disable, defaults to empty array)
				online: <true/false>, (disable, defaults to false)
				available: <true/false>, (disable, defaults to false)
				current_vehicle: <true/false>, (disable, defaults to null)
			} // Next employee
		],
		 emergencies: [
			{
				title: "",
				employees_onLocation: [<steam_ids>],
				notes: ``
			} // Next emergency
		 ],
		 groups: [
			 {
				 leader: <steam id of leader>
				 squad: [<steam_id>]
			 } // Next group
		 ]
	 }, // Next department
 }
 */
class DataManager {

	constructor(dataSaveLocation) {
		let moduleName = "dataManager [constructor]"
		if (isEmpty(dataSaveLocation)) {
			throw new MissingArgument("dataSaveLocation", moduleName)
		}
		this.fileLoc = dataSaveLocation
		if (!fs.existsSync(this.fileLoc)) {
			this.create(this.fileLoc)
		} else {
			this.load()
		}
	}

	async create(dataSaveLocation) {
		await touch(dataSaveLocation)
		this.data = {}
		for (let i = 0; i < validDepartments.length; i++) {
			const department = validDepartments[i];
			this.data[department] = {
				employers: [],
				groups: [],
				emergencies: []
			}
		}
		this.save()
	}

	async load() {
		let moduleName = "dataManager.load()"
		if (!fs.existsSync(this.fileLoc)) {
			throw new MissingFile(this.fileLoc, moduleName)
		}
		this.data = JSON.parse(await readFileSync(this.fileLoc))
		return true
	}

	async save() {
		let moduleName = "dataManager.save()"
		if (!fs.existsSync(this.fileLoc)) {
			throw new MissingFile(this.fileLoc, moduleName)
		}
		await writeFileSync(this.fileLoc, JSON.stringify(this.data))
		return true
	}

	getData() {
		return this.data
	}

	setData(data) {
		this.data = data
		return this.save()
	}

	// Tools
	getStatusCodeByDepartment(department, codeName) {
		switch (department) {
			case "police":
				switch (codeName.toLowerCase()) {
					case "beschikbaar":
					case "op post":
						return 1
					case "aanrijdend":
						return 2
					case "ter plaatsen":
						return 3
					case "spraakcontact speciaal":
						return 4
					case "melding afgerond":
					case "vertrekt":
						return 5
					case "spraakcontact oc":
					case "spraakcontact":
						return 6
					case "spraakcontact urgent":
						return 7
					case "niet beschikbaar":
						return 8
					case "uit dienst":
						return 9
					case "noodknop":
						return 0
					default:
						throw new IncorrectArgument("codeName", "Must be a valid status code name.")
				}
			case "ambulance":
			case "firefigter":
				switch (codeName.toLowerCase()) {
					case "beschikbaar":
						return 8
					case "op post":
						return 5
					case "aanrijdend":
						return 1
					case "ter plaatsen":
						return 2
					case "spraakcontact speciaal":
						return 6
					case "melding afgerond":
					case "vertrekt":
						return 3
					case "spraakcontact oc":
					case "spraakcontact":
						return 6
					case "spraakcontact urgent":
						return 7
					case "niet beschikbaar":
						return 4
					case "uit dienst":
						return 9
					case "noodknop":
						return 0
					default:
						throw new IncorrectArgument("codeName", "Must be a valid status code name.")
				}
			default:
				throw new IncorrectArgument("department", "Given Department is invalid.")
		}
	}
	// API Endpoints
	getEmployers(department) {
		let moduleName = "dataManager.getEmployers()"
		if (isEmpty(department)) throw new MissingArgument("department", moduleName)
		return this.data[department].employers
	}

	findEmployeeBySteamID(department, steamID) {
		let moduleName = "dataManager.findEmployeeBySteamID()"
		if (isEmpty(department)) throw new MissingArgument("department", moduleName)
		if (isEmpty(steamID)) throw new MissingArgument("steamID", moduleName)
		if (validDepartments.indexOf(department) == -1) throw new IncorrectArgument("department", `Valid departments are: ${validDepartments.toString()}`, moduleName)
		let allDepartEmployers = this.getEmployers(department)
		let res = allDepartEmployers[allDepartEmployers.findIndex(employee => employee.steam_id === steamID)]
		if (isEmpty(res)) throw new NotExist(moduleName)
		return res
	}

	findIDofEmployeeBySteamID(department, steamID) {
		let moduleName = "dataManager.getIDofEmployeeBySteamID()"
		if (isEmpty(department)) throw new MissingArgument("department", moduleName)
		if (isEmpty(steamID)) throw new MissingArgument("steamID", moduleName)
		if (validDepartments.indexOf(department) == -1) throw new IncorrectArgument("department", `Valid departments are: ${validDepartments.toString()}`, moduleName)
		let allDepartEmployers = this.getEmployers(department)
		let res = allDepartEmployers.findIndex(employee => employee.steam_id === steamID)
		if (res == -1) throw new NotExist(moduleName)
		return res
	}

	findSteamIDOfEmployeeByCallsign(department, callSignSearch) {
		let moduleName = "dataManager.findSteamIDOfEmployeeByCallsign()"
		if (isEmpty(department)) throw new MissingArgument("department", moduleName)
		if (isEmpty(callSignSearch)) throw new MissingArgument("callSign", moduleName)
		if (validDepartments.indexOf(department) == -1) throw new IncorrectArgument("department", `Valid departments are: ${validDepartments.toString()}`, moduleName)
		// Actual function code
		for (let i = 0; i < this.data[department].employers.length; i++) {
			const employee = this.data[department].employers[i];
			for (const roleName in employee.callsigns) {
				if (employee.callsigns.hasOwnProperty(roleName)) {
					const callSign = employee.callsigns[roleName];
					if (roleName == "default") continue
					if (!isEmpty(callSign.callsign) && callSign.callsign.replace("-", "") == callSignSearch.replace("-", "")) return employee.steam_id
				}
			}
		}
		return null
	}

	isEmployee(department, steamID) {
		try {
			this.findEmployeeBySteamID(department, steamID)
			return true
		} catch (e) {
			if (e.constructor.name == "NotExist") return false
			throw e
		}
	}

	addEmployee(department, employeeData) {
		let moduleName = "dataManager.addEmployee()"
		if (isEmptyAny([employeeData["rp_name"], employeeData["rang"], employeeData["steam_id"], employeeData["callsign"]])) throw new MissingArgument("employeeData", moduleName)
		if (isEmpty(department)) throw new MissingArgument("department", moduleName)
		if (validDepartments.indexOf(department) == -1) throw new IncorrectArgument("department", `Valid departments are: ${validDepartments.toString()}`, moduleName)
		if (this.isEmployee(department, employeeData["steam_id"])) throw new Exists("employee", moduleName)
		let currentDefaultRole
		switch (department) {
			case "police":
				currentDefaultRole = "Agent"
				break;
			case "ambulance":
				currentDefaultRole = "Ambulancier"
				break;
			case "firefighter":
				currentDefaultRole = "Brandweer"
				break;
			default:
				break;
		}
		this.data[department].employers.push({
			rp_name: employeeData["rp_name"],
			rang: employeeData["rang"],
			steam_id: employeeData["steam_id"],
			main_callsign: parseInt(employeeData["callsign"].replace("-", "")),
			callsigns: {
				default: currentDefaultRole,
				[currentDefaultRole]: {
					callsign: employeeData["callsign"],
					identity: employeeData["rp_name"]
				}
			},
			current_role: currentDefaultRole,
			specials: [],
			online: false,
			status: 9,
			statusReason: null,
			current_vehicle: null
		})
		this.save()
		return this.findEmployeeBySteamID(department, employeeData["steam_id"])
	}

	updateMainCallsign(department, steamID) {
		let moduleName = "dataManager.updateMainCallsign()"
		if (isEmpty(department)) throw new MissingArgument("department", moduleName)
		if (isEmpty(steamID)) throw new MissingArgument("steamID", moduleName)
		if (validDepartments.indexOf(department) == -1) throw new IncorrectArgument("department", `Valid departments are: ${validDepartments.toString()}`, moduleName)
		let employee = this.data[department].employers[this.findIDofEmployeeBySteamID(department, steamID)]
		this.data[department].employers[this.findIDofEmployeeBySteamID(department, steamID)]["main_callsign"] = parseInt(employee.callsigns[employee.callsigns.default].callsign.replace("-", ""))
		this.save()
		return this.data[department].employers[this.findIDofEmployeeBySteamID(department, steamID)]["main_callsign"]
	}

	editEmployeeBySteamID(department, steamID, field, innerField, value = null, groupCheck = false) {
		let moduleName = "dataManager.editEmployeeBySteamID()"
		if (isEmpty(field)) throw new MissingArgument("field", moduleName)
		if (isEmpty(department)) throw new MissingArgument("department", moduleName)
		if (validDepartments.indexOf(department) == -1) throw new IncorrectArgument("department", `Valid departments are: ${validDepartments.toString()}`, moduleName)
		let employee = this.findEmployeeBySteamID(department, steamID)
		if (!employee.hasOwnProperty(field)) throw new IncorrectArgument(field, `Field '${field}' isn't a valid field in employees.`, moduleName)
		let employeeID = this.findIDofEmployeeBySteamID(department, steamID)
		// Handle array values
		if (field == "specials") {
			if (value == "null" || isEmpty(value)) {
				this.data[department].employers[employeeID][field] = []
			} else {
				this.data[department].employers[employeeID][field] = JSON.parse(value.split(","))
			}
		// Handle object values
		} else if (field == "callsigns") {
			if (isEmpty(innerField)) {
				throw new MissingArgument("innerField", moduleName)
			}
			if (innerField == "default") {
				this.data[department].employers[employeeID][field][innerField] = value
			} else if (isEmpty(value) || value == "null") {
				delete this.data[department].employers[employeeID][field][innerField]
			} else {
				this.data[department].employers[employeeID][field][innerField] = JSON.parse(value)
			}
		// Handle number values
		} else if (field == "status") {
			this.data[department].employers[employeeID][field] = parseInt(value)
			if (this.data[department].employers[employeeID].online === true && parseInt(value) == this.getStatusCodeByDepartment(department, "uit dienst")) {
				let sI = this.inGroup(department, employee.steam_id)
				if (sI != -1) {
					this.removeSquadMember(department, employee.steam_id, this.data[department].groups[sI].uid)
				}
				this.data[department].employers[employeeID].online = false
			} else if (this.data[department].employers[employeeID].online === false && parseInt(value) != this.getStatusCodeByDepartment(department, "uit dienst")) {
				this.data[department].employers[employeeID].online = true
			}
		// Handle boolean values
		} else if (field == "online") {
			this.data[department].employers[employeeID][field] = (value == "true")
			if (this.data[department].employers[employeeID][field] === true) {
				this.data[department].employers[employeeID].status = this.getStatusCodeByDepartment(department, "niet beschikbaar")
				this.data[department].employers[employeeID].statusReason = "Niet aangemeld"
			} else if (this.data[department].employers[employeeID][field] === false) {
				let sI = this.inGroup(department, employee.steam_id)
				if (sI != -1) {
					this.removeSquadMember(department, employee.steam_id, this.data[department].groups[sI].uid)
				}
				this.data[department].employers[employeeID].status = this.getStatusCodeByDepartment(department, "uit dienst")
				this.data[department].employers[employeeID].statusReason = null
			} else {
				throw new IncorrectArgument("field", "Given field value has to be a boolean!")
			}
		// Handle anything else
		} else {
			// Check if field isn't protected
			if (field != "main_callsign") {
				if (value == "null" || isEmpty(value)) {
					// Required fields
					if (["rp_name", "steam_id", "rang"].indexOf(field) != -1) throw new IncorrectArgument("field", "Given field value can't be empty!")
					this.data[department].employers[employeeID][field] = null
				} else {
					this.data[department].employers[employeeID][field] = value
				}
			} else {
				throw new IncorrectArgument("field", "Given field is protected!", moduleName)
			}
		}
		// Post editing
		if (field == "callsigns") this.updateMainCallsign(department, steamID)
		if (field == "rp_name") this.data[department].employers[employeeID].callsigns[employee.callsigns.default].identity = value
		// Group checking
		if (!groupCheck) {
			// If non-grouped value, skip
			if (["rp_name", "rang", "steam_id", "main_callsign", "callsigns", "specials", "online"].indexOf(field) == -1) {
				if (field != "status" || (field == "status" && value != 9)) {
					let sI = this.inGroup(department, steamID)
					if (sI != -1) {
						for (let i = 0; i < this.data[department].groups[sI].squad.length; i++) {[department]
							const squadSteamID = this.data[department].groups[sI].squad[i];
							this.editEmployeeBySteamID(department, squadSteamID, field, innerField, value, true)
						}
					}
				}
			}
		}
		this.save()
		return this.findEmployeeBySteamID(department, steamID)
	}

	deleteEmployee(department, steamID) {
		let moduleName = "dataManager.deleteEmployee()"
		if (isEmpty(department)) throw new MissingArgument("department", moduleName)
		if (isEmpty(steamID)) throw new MissingArgument("steamID", moduleName)
		if (validDepartments.indexOf(department) == -1) throw new IncorrectArgument("department", `Valid departments are: ${validDepartments.toString()}`, moduleName)
		if (!this.isEmployee(department, steamID)) return true
		this.data[department].employers.splice(this.findIDofEmployeeBySteamID(department, steamID), 1)
		this.save()
		return true
	}

	getGroups(department) {
		let moduleName = "dataManager.getGroups()"
		if (isEmpty(department)) throw new MissingArgument("department", moduleName)
		if (validDepartments.indexOf(department) == -1) throw new IncorrectArgument("department", `Valid departments are: ${validDepartments.toString()}`, moduleName)
		return this.data[department].groups
	}

	getSquadIDByUID(department, groupID) {
		let moduleName = "dataManager.getSquadIDBySteamID()"
		if (isEmpty(groupID)) throw new MissingArgument("groupID", moduleName)
		if (isEmpty(department)) throw new MissingArgument("department", moduleName)
		if (validDepartments.indexOf(department) == -1) throw new IncorrectArgument("department", `Valid departments are: ${validDepartments.toString()}`, moduleName)
		let res = this.data[department].groups.findIndex(group => group.uid == groupID)
		if (res == -1) throw new NotExist(moduleName, `No emergency couldn't be found by the group ID of: ${groupID}`)
		return this.data[department].emergencies[res].uid
	}

	getSquadIndexByUID(department, groupID) {
		let moduleName = "dataManager.getSquadIDBySteamID()"
		if (isEmpty(groupID)) throw new MissingArgument("groupID", moduleName)
		if (isEmpty(department)) throw new MissingArgument("department", moduleName)
		if (validDepartments.indexOf(department) == -1) throw new IncorrectArgument("department", `Valid departments are: ${validDepartments.toString()}`, moduleName)
		let res = this.data[department].groups.findIndex(group => group.uid == groupID)
		if (res == -1) throw new NotExist(moduleName, `No emergency couldn't be found by the group ID of: ${groupID}`)
		return res
	}

	getSquadBySteamID(department, steamID) {
		let moduleName = "dataManager.getSquadBySteamID()"
		if (isEmpty(steamID)) throw new MissingArgument("steamID", moduleName)
		if (isEmpty(department)) throw new MissingArgument("department", moduleName)
		if (validDepartments.indexOf(department) == -1) throw new IncorrectArgument("department", `Valid departments are: ${validDepartments.toString()}`, moduleName)
		// Check if steamID exists
		if (!this.isEmployee(department, steamID)) {
			throw new NotExist(moduleName, `Some steamID's couldn't be found in the employee database under the department of ${department}.`)
		}
		let res = this.data[department].groups.findIndex(group => group.squad.indexOf(steamID) != -1)
		if (res == -1) throw new NotExist(moduleName, `No emergency couldn't be found by the steam id of: ${steamID}`)
		return this.data[department].groups[res]
	}

	getSquadUIDBySteamID(department, steamID) {
		let moduleName = "dataManager.getSquadUIDBySteamID()"
		if (isEmpty(steamID)) throw new MissingArgument("steamID", moduleName)
		if (isEmpty(department)) throw new MissingArgument("department", moduleName)
		if (validDepartments.indexOf(department) == -1) throw new IncorrectArgument("department", `Valid departments are: ${validDepartments.toString()}`, moduleName)
		// Check if steamID exists
		if (!this.isEmployee(department, steamID)) {
			throw new NotExist(moduleName, `Some steamID's couldn't be found in the employee database under the department of ${department}.`)
		}
		let res = this.data[department].groups.findIndex(group => group.squad.indexOf(steamID) != -1)
		if (res == -1) throw new NotExist(moduleName, `No emergency couldn't be found by the steam id of: ${steamID}`)
		return this.data[department].groups[res].uid
	}

	getSquadUIDByIndex(department, index) {
		let moduleName = "dataManager.getSquadUIDByIndex()"
		if (isEmpty(index)) throw new MissingArgument("steamID", moduleName)
		if (isEmpty(department)) throw new MissingArgument("department", moduleName)
		if (validDepartments.indexOf(department) == -1) throw new IncorrectArgument("department", `Valid departments are: ${validDepartments.toString()}`, moduleName)
		// Actual function code
		let res = this.data[department].groups[index]
		if (isEmpty(res)) throw new NotExist(moduleName, `No emergency couldn't be found by the index of: ${index}`)
		return res.uid
	}

	inGroup(department, steamID, groupID) {
		let moduleName = "dataManager.inGroup()"
		if (isEmpty(steamID)) throw new MissingArgument("steamID", moduleName)
		if (isEmpty(department)) throw new MissingArgument("department", moduleName)
		if (validDepartments.indexOf(department) == -1) throw new IncorrectArgument("department", `Valid departments are: ${validDepartments.toString()}`, moduleName)
		// Check if the steamID exists
		if (!this.isEmployee(department, steamID)) {
			throw new NotExist(moduleName, "The given steamID couldn't be found in the employee database.")
		}

		let res = -1
		if (!isEmpty(groupID)) {
			if (!this.isGroup(department, groupID)) throw new NotExist(moduleName, "The group couldn't be found")
			let sI = this.getSquadIndexByUID(department, groupID)
			res = this.data[department].groups[sI].squad.indexOf(steamID)
		} else {
			for (let i = 0; i < this.data[department].groups.length; i++) {
				const group = this.data[department].groups[i];
				if (group.squad.indexOf(steamID) != -1) {
					res = i
					break
				}
			}
		}
		return res
	}

	isGroup(department, groupID) {
		let moduleName = "dataManager.isGroup()"
		if (isEmpty(department)) throw new MissingArgument("department", moduleName)
		if (isEmpty(groupID)) throw new MissingArgument("groupID", moduleName)
		if (validDepartments.indexOf(department) == -1) throw new IncorrectArgument("department", `Valid departments are: ${validDepartments.toString()}`, moduleName)
		return (this.data[department].groups.findIndex(group => group.uid == groupID) != -1)
	}

	deleteSquad(department, groupID) {
		let moduleName = "dataManager.deleteSquad()"
		if (isEmpty(department)) throw new MissingArgument("department", moduleName)
		if (isEmpty(groupID)) throw new MissingArgument("groupID", moduleName)
		if (validDepartments.indexOf(department) == -1) throw new IncorrectArgument("department", `Valid departments are: ${validDepartments.toString()}`, moduleName)
		if (!this.isGroup(department, groupID)) throw new NotExist(moduleName, "The group couldn't be found")
		let sI = this.getSquadIndexByUID(department, groupID)
		this.data[department].groups.splice(sI, 1)
		this.save()
		return true
	}

	addSquadMember(department, steamID, groupID) {
		let moduleName = "dataManager.addSquadMember()"
		if (isEmpty(steamID)) throw new MissingArgument("steamID", moduleName)
		if (isEmpty(department)) throw new MissingArgument("department", moduleName)
		if (isEmpty(groupID)) throw new MissingArgument("groupID", moduleName)
		if (validDepartments.indexOf(department) == -1) throw new IncorrectArgument("department", `Valid departments are: ${validDepartments.toString()}`, moduleName)
		if (!this.isGroup(department, groupID)) throw new NotExist(moduleName, "The group ID '" + groupID + "' couldn't be found")
		if (!this.isEmployee(department, steamID)) {
			throw new NotExist(moduleName, `The steamID couldn't be found in the employee database under the department of ${department}.`)
		}
		if (this.findEmployeeBySteamID(department, steamID).online === false) throw new IncorrectArgument("steamIDs", "Not all given employees are online. To make a group all given employees must be online!")
		let sI = this.getSquadIndexByUID(department, groupID)
		for (let i = 0; i < this.data[department].groups.length; i++) {
			const group = this.data[department].groups[i];
			if (group.uid == groupID) continue
			if (group.squad.indexOf(steamID) != -1) {
				this.removeSquadMember(department, steamID, group.uid)
				if (group.squad.length == 0) this.deleteSquad(department, group.uid)
			}
		}
		if (this.data[department].groups[sI].squad.indexOf(steamID) != -1) throw new Exists("Squad Member", moduleName)
		this.data[department].groups[sI].squad.push(steamID)
		this.save()
		return this.data[department].groups[sI]
	}

	addMultipleEmployeesToSquad(department, steamIDs, groupID) {
		let moduleName = "dataManager.addMultipleEmployeesToSquad()"
		if (isEmpty(steamIDs)) throw new MissingArgument("steamIDs", moduleName)
		steamIDs = JSON.parse(steamIDs)
		if (isEmpty(department)) throw new MissingArgument("department", moduleName)
		if (isEmpty(groupID)) throw new MissingArgument("groupID", moduleName)
		if (validDepartments.indexOf(department) == -1) throw new IncorrectArgument("department", `Valid departments are: ${validDepartments.toString()}`, moduleName)
		if (!this.isGroup(department, groupID)) throw new NotExist(moduleName, "The given group ID is invalid.")
		for (let i = 0; i < steamIDs.length; i++) {
			const steamID = steamIDs[i];
			this.addSquadMember(department, steamID, groupID)
		}
		this.save()
		return this.getSquadBySteamID(department, steamIDs[0])
	}

	removeSquadMember(department, steamID, groupID) {
		let moduleName = "dataManager.removeSquadMember()"
		if (isEmpty(steamID)) throw new MissingArgument("steamID", moduleName)
		if (isEmpty(department)) throw new MissingArgument("department", moduleName)
		if (isEmpty(groupID)) throw new MissingArgument("groupID", moduleName)
		if (validDepartments.indexOf(department) == -1) throw new IncorrectArgument("department", `Valid departments are: ${validDepartments.toString()}`, moduleName)
		if (!this.isGroup(department, groupID)) throw new NotExist(moduleName, "The group ID '" + groupID + "' couldn't be found")
		let sI = this.getSquadIndexByUID(department, groupID)
		this.data[department].groups[sI].squad.removeByValue(steamID)
		if (this.data[department].groups[sI].squad.length <= 1) return this.deleteSquad(department, groupID)
		if (this.data[department].groups[sI].leader == steamID) this.data[department].groups[sI].leader = this.data[department].groups[sI].squad[0]
		this.save()
		return this.data[department].groups[sI]
	}

	setSquadLeader(department, steamID, groupID) {
		let moduleName = "dataManager.setSquadLeader()"
		if (isEmpty(steamID)) throw new MissingArgument("steamID", moduleName)
		if (isEmpty(groupID)) throw new MissingArgument("groupID", moduleName)
		if (isEmpty(department)) throw new MissingArgument("department", moduleName)
		if (validDepartments.indexOf(department) == -1) throw new IncorrectArgument("department", `Valid departments are: ${validDepartments.toString()}`, moduleName)

		let sI = this.getSquadIndexByUID(department, groupID)
		if (this.inGroup(department, steamID, groupID) == -1) this.addSquadMember(department, steamID, groupID)
		this.data[department].groups[sI].leader = steamID

		this.save()
		return this.data[department].groups[sI]
	}

	setSquad(department, steamIDs, leaderSteamID) {
		let moduleName = "dataManager.setSquad()"
		steamIDs = JSON.parse(steamIDs)
		if (isEmpty(steamIDs)) throw new MissingArgument("steamIDs", moduleName)
		if (isEmpty(leaderSteamID)) throw new MissingArgument("leaderSteamID", moduleName)
		if (isEmpty(department)) throw new MissingArgument("department", moduleName)
		if (validDepartments.indexOf(department) == -1) throw new IncorrectArgument("department", `Valid departments are: ${validDepartments.toString()}`, moduleName)
		// Check if all steamIDs exists
		for (let i = 0; i < steamIDs.length; i++) {
			const steamID = steamIDs[i];
			if (!this.isEmployee(department, steamID)) {
				throw new NotExist(moduleName, `Some steamID's couldn't be found in the employee database under the department of ${department}.`)
			}
		}
		// Check if leaderSteamID is part of the steamIDs array
		if (steamIDs.indexOf(leaderSteamID) == -1) throw new IncorrectArgument("leaderSteamID", "Leader isn't in it's own Squad team! Make sure to add the Leader's steam ID to the squad list")
		// Any of the steamIDs already in a group? (REMEMBER members listed in steamIDs that are in other groups will be removed in those groups)
		for (let i = 0; i < steamIDs.length; i++) {
			const steamID = steamIDs[i];
			const employee = this.findEmployeeBySteamID(department, steamID)
			if (employee.online === false) throw new IncorrectArgument("steamIDs", "Not all given employees are online. To make a group all given employees must be online!")
			let resGroupID = this.inGroup(department, steamID)
			if (resGroupID == -1) continue
			let resRSM = this.removeSquadMember(department, steamID, this.data[department].groups[resGroupID].uid)
			if (typeof resRSM == "boolean" && resRSM === false) throw new ImpossibleState(moduleName, null, `removeSquadMember can't return false!`)
		}
		// Set the squad in database
		let uidGen = new Date()
		this.data[department].groups.push({
			uid: parseInt(`${uidGen.getDate()}${uidGen.getHours()}${uidGen.getMinutes()}${uidGen.getSeconds()}${uidGen.getMilliseconds()}`),
			leader: leaderSteamID,
			squad: steamIDs
		})
		this.save()
		return this.data[department].groups[this.data[department].groups.length-1]
	}

	getEmergencies(department) {
		let moduleName = "dataManager.getEmergencies()"
		if (isEmpty(department)) throw new MissingArgument("department", moduleName)
		if (validDepartments.indexOf(department) == -1) throw new IncorrectArgument("department", `Valid departments are: ${validDepartments.toString()}`, moduleName)
		return this.data[department].emergencies
	}

	isValidEmergency(department, emergencyID) {
		let moduleName = "dataManager.isValidEmergency()"
		if (isEmpty(emergencyID)) throw new MissingArgument("emergencyID", moduleName)
		if (isEmpty(department)) throw new MissingArgument("department", moduleName)
		if (validDepartments.indexOf(department) == -1) throw new IncorrectArgument("department", `Valid departments are: ${validDepartments.toString()}`, moduleName)
		// Actual Function Code
		return (this.data[department].emergencies.findIndex(emergency => emergency.uid == emergencyID) != -1)
	}

	addEmergency(department, steamIDs) {
		let moduleName = "dataManager.addEmergency()"
		steamIDs = JSON.parse(steamIDs)
		if (isEmpty(steamIDs)) throw new MissingArgument("steamIDs", moduleName)
		if (isEmpty(department)) throw new MissingArgument("department", moduleName)
		if (validDepartments.indexOf(department) == -1) throw new IncorrectArgument("department", `Valid departments are: ${validDepartments.toString()}`, moduleName)
		// Check if all steamIDs exists
		for (let i = 0; i < steamIDs.length; i++) {
			const steamID = steamIDs[i];
			if (!this.isEmployee(department, steamID)) {
				throw new NotExist(moduleName, `Some steamID's couldn't be found in the employee database under the department of ${department}.`)
			}
		}
		// Actual function code
		// Check if steamID's aren't in another emergency. If so remove them there and add them here
		for (let i = 0; i < this.data[department].emergencies.length; i++) {
			const emergency = this.data[department].emergencies[i];
			for (let j = 0; j < steamIDs.length; j++) {
				const steamID = steamIDs[j];
				let k = emergency.employees_onLocation.indexOf(steamID)
				if (k != -1) {
					emergency.employees_onLocation.splice(k, 1)
					if (emergency.employees_onLocation.length == 0) this.deleteEmergency(department, emergency.uid)
					break
				}
			}
		}
		let uidGen = new Date()
		this.data[department].emergencies.push({
			uid: parseInt(`${uidGen.getDate()}${uidGen.getHours()}${uidGen.getMinutes()}${uidGen.getSeconds()}${uidGen.getMilliseconds()}`),
			title: "Onbekende Melding",
			employees_onLocation: steamIDs,
			notes: ``
		})
		for (let i = 0; i < steamIDs.length; i++) {
			const steamID = steamIDs[i];
			this.editEmployeeBySteamID(department, steamID, "status", null, this.getStatusCodeByDepartment(department, "aanrijdend"))
		}
		this.save()
		return this.data[department].emergencies[this.data[department].emergencies.length-1]
	}

	deleteEmergency(department, emergencyID) {
		let moduleName = "dataManager.deleteEmergency()"
		if (isEmpty(emergencyID)) throw new MissingArgument("steamIDs", moduleName)
		if (isEmpty(department)) throw new MissingArgument("department", moduleName)
		if (validDepartments.indexOf(department) == -1) throw new IncorrectArgument("department", `Valid departments are: ${validDepartments.toString()}`, moduleName)
		if (!this.isValidEmergency(department, emergencyID)) return true
		// Actual function code
		let eI = this.getEmergencyIndexByUID(department, emergencyID)
		for (let i = 0; i < this.data[department].emergencies[eI].employees_onLocation.length; i++) {
			const steamID = this.data[department].emergencies[eI].employees_onLocation[i];
			this.editEmployeeBySteamID(department, steamID, "status", null, this.getStatusCodeByDepartment(department, "melding afgerond"))
		}
		this.data[department].emergencies.splice(eI, 1)
		this.save()
		return true
	}

	getEmergencyIndexByUID(department, emergencyID) {
		let moduleName = "dataManager.getEmergencyIndexByUID()"
		if (isEmpty(emergencyID)) throw new MissingArgument("emergencyID", moduleName)
		if (isEmpty(department)) throw new MissingArgument("department", moduleName)
		if (validDepartments.indexOf(department) == -1) throw new IncorrectArgument("department", `Valid departments are: ${validDepartments.toString()}`, moduleName)
		if (!this.isValidEmergency(department, emergencyID)) throw new NotExist(moduleName, "The given emergency ID is invalid.")
		let res = this.data[department].emergencies.findIndex(emergency => emergency.uid == emergencyID)
		if (res == -1) throw new NotExist(moduleName, `No emergency couldn't be found by the steam id of: ${steamID}`)
		return res
	}

	getEmergencyBySteamID(department, steamID) {
		let moduleName = "dataManager.getEmergencyBySteamID()"
		if (isEmpty(steamID)) throw new MissingArgument("steamID", moduleName)
		if (isEmpty(department)) throw new MissingArgument("department", moduleName)
		if (validDepartments.indexOf(department) == -1) throw new IncorrectArgument("department", `Valid departments are: ${validDepartments.toString()}`, moduleName)
		// Check if steamID exists
		if (!this.isEmployee(department, steamID)) {
			throw new NotExist(moduleName, `Some steamID's couldn't be found in the employee database under the department of ${department}.`)
		}
		let res = this.data[department].emergencies.findIndex(emergency => emergency.employees_onLocation.indexOf(steamID) != -1)
		if (res == -1) throw new NotExist(moduleName, `No emergency couldn't be found by the steam id of: ${steamID}`)
		return this.data[department].emergencies[res]
	}

	getEmergencyIDBySteamID(department, steamID) {
		let moduleName = "dataManager.getEmergencyIDBySteamID()"
		if (isEmpty(steamID)) throw new MissingArgument("steamID", moduleName)
		if (isEmpty(department)) throw new MissingArgument("department", moduleName)
		if (validDepartments.indexOf(department) == -1) throw new IncorrectArgument("department", `Valid departments are: ${validDepartments.toString()}`, moduleName)
		// Check if steamID exists
		if (!this.isEmployee(department, steamID)) {
			throw new NotExist(moduleName, `Some steamID's couldn't be found in the employee database under the department of ${department}.`)
		}
		let res = this.data[department].emergencies.findIndex(emergency => emergency.employees_onLocation.indexOf(steamID) != -1)
		if (res == -1) throw new NotExist(moduleName, `No emergency couldn't be found by the steam id of: ${steamID}`)
		return this.data[department].emergencies[res].uid
	}

	saveEmergencyData(department, emergencyID, title, notes) {
		let moduleName = "dataManager.saveEmergencyData()"
		if (isEmpty(title)) throw new MissingArgument("title", moduleName)
		if (isEmpty(notes)) throw new MissingArgument("notes", moduleName)
		if (isEmpty(department)) throw new MissingArgument("department", moduleName)
		if (isEmpty(emergencyID)) throw new MissingArgument("emergencyID", moduleName)
		if (validDepartments.indexOf(department) == -1) throw new IncorrectArgument("department", `Valid departments are: ${validDepartments.toString()}`, moduleName)
		if (!this.isValidEmergency(department, emergencyID)) throw new NotExist(moduleName, "The given emergency ID is invalid.")
		// Actual Function Code
		let thisEmergencyIndex = this.getEmergencyIndexByUID(department, emergencyID)
		this.data[department].emergencies[thisEmergencyIndex].title = title
		this.data[department].emergencies[thisEmergencyIndex].notes = notes
		this.save()
		return this.data[department].emergencies[thisEmergencyIndex]
	}

	addEmployeeToEmergency(department, steamID, emergencyID, saveAfterwards = true) {
		let moduleName = "dataManager.addEmployeeToEmergency()"
		if (isEmpty(steamID)) throw new MissingArgument("steamID", moduleName)
		if (isEmpty(department)) throw new MissingArgument("department", moduleName)
		if (isEmpty(emergencyID)) throw new MissingArgument("emergencyID", moduleName)
		if (validDepartments.indexOf(department) == -1) throw new IncorrectArgument("department", `Valid departments are: ${validDepartments.toString()}`, moduleName)
		if (!this.isValidEmergency(department, emergencyID)) throw new NotExist(moduleName, "The given emergency ID is invalid.")
		// Check if steamID exists
		if (!this.isEmployee(department, steamID)) {
			throw new NotExist(moduleName, `Some steamID's couldn't be found in the employee database under the department of ${department}.`)
		}
		// Actual Function Code
		// Check if steamID's aren't in another emergency. If so remove them there and add them here
		for (let i = 0; i < this.data[department].emergencies.length; i++) {
			const emergency = this.data[department].emergencies[i];
			if (emergency.uid == emergencyID) continue
			let j = emergency.employees_onLocation.indexOf(steamID)
			if (j != -1) {
				emergency.employees_onLocation.splice(j, 1)
				if (emergency.employees_onLocation.length == 0) {
					this.deleteEmergency(department, emergency.uid)
				}
				break
			}
		}
		let thisEmergencyIndex = this.getEmergencyIndexByUID(department, emergencyID)
		if (this.data[department].emergencies[thisEmergencyIndex].employees_onLocation.indexOf(steamID) == -1) {
			this.data[department].emergencies[thisEmergencyIndex].employees_onLocation.push(steamID)
			if (saveAfterwards === true) this.save()
		}
		return this.data[department].emergencies[thisEmergencyIndex]
	}

	removeEmployeeFromEmergency(department, steamID, emergencyID) {
		let moduleName = "dataManager.removeEmployeeFromEmergency()"
		if (isEmpty(steamID)) throw new MissingArgument("steamID", moduleName)
		if (isEmpty(department)) throw new MissingArgument("department", moduleName)
		if (isEmpty(emergencyID)) throw new MissingArgument("emergencyID", moduleName)
		if (validDepartments.indexOf(department) == -1) throw new IncorrectArgument("department", `Valid departments are: ${validDepartments.toString()}`, moduleName)
		if (!this.isValidEmergency(department, emergencyID)) throw new NotExist(moduleName, "The given emergency ID is invalid.")
		// Check if steamID exists
		if (!this.isEmployee(department, steamID)) {
			throw new NotExist(moduleName, `Some steamID's couldn't be found in the employee database under the department of ${department}.`)
		}
		// Actual Function Code
		let thisEmergencyIndex = this.getEmergencyIndexByUID(department, emergencyID)
		if (this.data[department].emergencies[thisEmergencyIndex].employees_onLocation.indexOf(steamID) != -1) {
			this.data[department].emergencies[thisEmergencyIndex].employees_onLocation.removeByValue(steamID)
			this.editEmployeeBySteamID(department, steamID, "status", null, this.getStatusCodeByDepartment(department, "melding afgerond"))
			this.save()
			if (this.data[department].emergencies[thisEmergencyIndex].employees_onLocation.length == 0) {
				this.deleteEmergency(department, this.data[department].emergencies[thisEmergencyIndex].uid)
				return null
			}
		}
		return this.data[department].emergencies[thisEmergencyIndex]
	}

	addMultipleEmployeesToEmergency(department, steamIDs, emergencyID) {
		let moduleName = "dataManager.addMultipleEmployeesToEmergency()"
		steamIDs = JSON.parse(steamIDs)
		if (isEmpty(steamIDs)) throw new MissingArgument("steamIDs", moduleName)
		if (isEmpty(department)) throw new MissingArgument("department", moduleName)
		if (isEmpty(emergencyID)) throw new MissingArgument("emergencyID", moduleName)
		if (validDepartments.indexOf(department) == -1) throw new IncorrectArgument("department", `Valid departments are: ${validDepartments.toString()}`, moduleName)
		if (!this.isValidEmergency(department, emergencyID)) throw new NotExist(moduleName, "The given emergency ID is invalid.")
		for (let i = 0; i < steamIDs.length; i++) {
			const steamID = steamIDs[i];
			this.addEmployeeToEmergency(department, steamID, emergencyID, false)
		}
		this.save()
		let thisEmergencyIndex = this.getEmergencyIndexByUID(department, emergencyID)
		return this.data[department].emergencies[thisEmergencyIndex]
	}

}

// Loading config variables
var expressConfig
if (!fs.existsSync("./config.js")) {
	debugHand.error("Config file couldn't be found. Loading default from hard-code...", moduleName)
	expressConfig = {
		port: 8000,
		host: "127.0.0.1",
		adminWhitelist: [
			"127.0.0.1"
		]
	}
} else {
	expressConfig = require("./config.js")
	debugHand.log("Config loaded from file!", 1, moduleName)
}

// Config location/files check
// Create/Load the config file
var dataBin
try {
	dataBin = new DataManager(dataSaveLocation)
	debugHand.log("Created/Loaded data bin...", 1, moduleName)
} catch (e) {
	if (e instanceof DroidsError) {
		debugHand.error(e.errMsg, e.moduleName, e.stack)
	} else {
		debugHand.error(e.message, moduleName, e.stack)
	}
	debugHand.error("Exiting...")
	process.exit(1)
}

// General functions - tmp.gfunc
/**
* My famous (not really) isEmpty function. It checks if a given variable is empty or not
* @param {*} variable Anything you want to know if it's empty or not
* @returns {Boolean} Returns true on empty and false on not empty
*/
function isEmpty(variable) {
if (variable == null || variable == undefined) return true
	if (typeof variable == "string") {
		if (variable == "") return true
	} else if (typeof variable == "object") {
		if (Array.isArray(variable)) {
			if (variable.length == 0) return true
		} else {
			if (Object.keys(variable).length == 0) return true
		}
	}
	return false
}

/**
* My second famous (not really) isEmptyAny function. It checks if any variable in a given array is empty or not
* @param {Array} array An array with elements you want to know if any of them are empty
* @returns {Boolean} Returns true on any of them being empty and false on none of them being empty
*/
function isEmptyAny(array) {
	for (let i = 0; i < array.length; i++) {
		if (isEmpty(array[i])) {
			return true
		}
	}
	return false
}

// Program's helper functions
function argumentBuilder(argumentObj, get, post) {
	if (isEmpty(argumentObj)) return []
	let arguments = []
	for (const method in argumentObj) {
		if (argumentObj.hasOwnProperty(method)) {
			const keyArray = argumentObj[method];
			if (method === "GET") {
				if (keyArray === "*") {
					arguments.push(get)
				} else if (Array.isArray(keyArray)) {
					for (let j = 0; j < keyArray.length; j++) {
						const key = keyArray[j];
						arguments.push(get[key])
					}
				} else continue
			} else if (method === "POST") {
				if (keyArray === "*") {
					arguments.push(post)
				} else if (Array.isArray(keyArray)) {
					for (let j = 0; j < keyArray.length; j++) {
						const key = keyArray[j];
						arguments.push(post[key])
					}
				} else continue
			} else continue
		}
	}
	return arguments
}

// Program specific variables
const app = express()

app.set('view engine', 'ejs');

app.use(express.json())
app.use(express.urlencoded({
	extended: true
}))

app.use("/css/", express.static(__dirname + '/assets/css'));
app.use("/fontawesome/", express.static(__dirname + '/assets/fontawesome'));
app.use("/js/", express.static(__dirname + '/assets/js'));
app.use("/img/", express.static(__dirname + '/assets/img'));
app.use("/fonts/", express.static(__dirname + '/assets/fonts'));
app.use("/favicon/", express.static(__dirname + '/assets/favicon'));

const validDepartments = [
	"police",
	"firefighter",
	"ambulance"
]

const displayedDepartments = {
	"police": "Politie",
	"firefighter": "Brandweer",
	"ambulance": "Ambulance"
}

var selfTestActive = false

// Non-API paths that are whitelisted to go to when self test is active
var selfTestWhitelistedPahts = []

// Program's (handler) functions
function error_page_handling(statusCode = 500, res) {
	let errorPage = `${__dirname}/assets/ejs/error-pages/${statusCode}.ejs`
	if (!fs.existsSync(errorPage)) {
		if (!fs.existsSync(`${__dirname}/assets/ejs/error-pages/404.ejs`)) {
			res.statusCode = 404
			res.end("Page not found. Not even the 404 page...")
		} else {
			res.statusCode = 404
			res.render(path.join(`${__dirname}/assets/ejs/error-pages/404.ejs`))
		}
	} else {
		res.statusCode = statusCode
		res.render(path.join(errorPage))
	}
}

function selfTestHandler(req, res) {
	if (selfTestActive) {
		let clientIP = req.headers['x-forwarded-for'] || req.connection.remoteAddress
		if (expressConfig.adminWhitelist.indexOf(clientIP) != -1 && req.path.startsWith("/api/")) return false
		error_page_handling(503, res)
		return true
	} else {
		return false
	}
}

function resAPIHandling(res, retVal = {}) {
	if (retVal.success == undefined) retVal.success = null
	if (retVal.data == undefined) retVal.data = null
	if (retVal.error == undefined) retVal.error = null
	res.statusCode = 200
	res.setHeader('Content-Type', 'application/json')
	res.end(JSON.stringify(retVal))
}

function apiHandling(req, res) {
	let moduleName = "apiHandling"
	const apiFuncs = {
		"findEmployee": "findEmployeeBySteamID",
		"findSteamIDByCallSign": "findSteamIDOfEmployeeByCallsign",
		"editEmployeeField": "editEmployeeBySteamID",
		"getSquads": "getGroups",
		"saveEmergency": "saveEmergencyData"
	}
	const argList = {
		"getEmployers": {
			"GET": ["department"]
		},
		"addEmployee": {
			"GET": ["department"],
			"POST": "*"
		},
		"findEmployee": {
			"GET": ["department"],
			"POST": ["value"]
		},
		"findSteamIDByCallSign": {
			"GET": ["department"],
			"POST": ["callsign"]
		},
		"editEmployeeField": {
			"GET": ["department", "steam_id"],
			"POST": ["field", "innerField", "value"]
		},
		"deleteEmployee": {
			"GET": ["department"],
			"POST": ["steam_id"]
		},
		"getSquads": {
			"GET": ["department"]
		},
		"setSquad": {
			"GET": ["department"],
			"POST": ["steam_ids", "leader_steam_id"]
		},
		"getSquadBySteamID": {
			"GET": ["department"],
			"POST": ["steam_id"]
		},
		"getSquadUIDBySteamID": {
			"GET": ["department"],
			"POST": ["steam_id"]
		},
		"getSquadUIDByIndex": {
			"GET": ["department"],
			"POST": ["index"]
		},
		"addSquadMember": {
			"GET": ["department"],
			"POST": ["steam_id", "group_id"]
		},
		"addMultipleEmployeesToSquad": {
			"GET": ["department"],
			"POST": ["steam_ids", "group_id"]
		},
		"removeSquadMember": {
			"GET": ["department"],
			"POST": ["steam_id", "group_id"]
		},
		"setSquadLeader": {
			"GET": ["department"],
			"POST": ["steam_id", "group_id"]
		},
		"deleteSquad": {
			"GET": ["department"],
			"POST": ["group_id"]
		},
		"getEmergencies": {
			"GET": ["department"]
		},
		"getEmergencyBySteamID": {
			"GET": ["department", "steam_id"]
		},
		"getEmergencyIDBySteamID": {
			"GET": ["department", "steam_id"]
		},
		"addEmergency": {
			"GET": ["department"],
			"POST": ["steam_ids"]
		},
		"deleteEmergency": {
			"GET": ["department"],
			"POST": ["emergency_id"]
		},
		"saveEmergency": {
			"GET": ["department"],
			"POST": ["emergency_id", "emergency_title", "emergency_notes"]
		},
		"addEmployeeToEmergency": {
			"GET": ["department"],
			"POST": ["steam_id", "emergency_id"]
		},
		"removeEmployeeFromEmergency": {
			"GET": ["department"],
			"POST": ["steam_id", "emergency_id"]
		},
		"addMultipleEmployeesToEmergency": {
			"GET": ["department"],
			"POST": ["steam_ids", "emergency_id"]
		},
	}
	let funcName = req.path.split("/")[2]
	if (isEmpty(funcName)) {
		debugHand.error("Function name couldn't be found.", moduleName, funcName)
		return resAPIHandling(res, {error: "Developer error: Function name couldn't be found.", success: false})
	}
	let func = (typeof dataBin[funcName] == "function") ? dataBin[funcName] : (!isEmpty(apiFuncs[funcName])) ? (typeof dataBin[apiFuncs[funcName]] == "function") ? dataBin[apiFuncs[funcName]] : null : null
	if (func === null) {
		debugHand.error("Function couldn't be found in the DataManager.", moduleName, funcName)
		return resAPIHandling(res, {error: "Developer error: Function couldn't be found in the DataManager.", success: false})
	}
	moduleName = `API.${req.path.split("/")[2]}`
	if (selfTestHandler(req, res)) return
	let retVal = {
		"success": null,
		"data": null,
		"error": null
	}
	let gQuery = req.query
	let query = req.body
	debugHand.log("Receiving query: ", 3, moduleName, req.query)
	debugHand.log("Receiving body: ", 3, moduleName, req.body)
	debugHand.log("Argument builder result: ", 3, moduleName, argumentBuilder(argList[funcName], gQuery, query))
	if (validDepartments.indexOf(gQuery["department"]) != -1) {
		try {
			retVal.data = func.apply(dataBin, argumentBuilder(argList[funcName], gQuery, query))
			retVal.success = true
		} catch (e) {
			if (e instanceof DroidsError) {
				debugHand.log(e.errMsg, 3, e.moduleName, e.stack)
				retVal.success = false
				retVal.error = e.errMsg
			} else {
				debugHand.error(e.message, moduleName, e.stack)
				retVal.success = false
				retVal.error = e.message
			}
		}
	} else {
		retVal.success = false
		retVal.error = `Invalid department. Valid departments are: ${validDepartments.toString()}`
	}
	resAPIHandling(res, retVal)
}

app.get("/", function(req, res) {
	if (selfTestHandler(req, res)) return
	if (validDepartments.indexOf(req.query["afdeling"]) != -1) {
		res.render(path.join(__dirname + "/assets/ejs/oc.ejs"), {department: req.query["afdeling"], displayedDepartment: displayedDepartments[req.query["afdeling"]], version: versionNumber, versionType: versionType})
	} else {
		res.render(path.join(__dirname + "/assets/ejs/index.ejs"))
	}
})

// TODO: UNCOMMENT OR REMOVE WHEN IN PRODUCTION!!!!
/* app.get("/api/test", function(req, res) {
	if (selfTestHandler(req, res)) return
	res.render(path.join(__dirname + "/assets/ejs/test.ejs"))
}) */

// Express's API Points
app.get("/key_printer", function(req, res) {
	if (selfTestHandler(req, res)) return
	res.render(path.join(__dirname + "/assets/ejs/key_printer.ejs"))
})

app.get("/api/getTest", function(req, res) {
	if (selfTestHandler(req, res)) return
	res.statusCode = 200
	res.setHeader('Content-Type', 'application/json')
	res.end(JSON.stringify(req.query))
})

app.post("/api/postTest", function(req, res) {
	if (selfTestHandler(req, res)) return
	res.statusCode = 200
	res.setHeader('Content-Type', 'application/json')
	res.end(JSON.stringify(req.body))
})

// Employers API
app.get("/api/getEmployers", apiHandling)

app.post("/api/addEmployee", apiHandling)

app.post("/api/findEmployee", apiHandling)

app.post("/api/findSteamIDByCallSign", apiHandling)

app.post("/api/editEmployeeField", apiHandling)

app.post("/api/deleteEmployee", apiHandling)

// Squad API
app.get("/api/getSquads", apiHandling)

app.post("/api/setSquad", apiHandling)

app.post("/api/getSquadBySteamID", apiHandling)

app.post("/api/getSquadUIDBySteamID", apiHandling)

app.post("/api/getSquadUIDByIndex", apiHandling)

app.post("/api/addSquadMember", apiHandling)

app.post("/api/addMultipleEmployeesToSquad", apiHandling)

app.post("/api/removeSquadMember", apiHandling)

app.post("/api/setSquadLeader", apiHandling)

app.post("/api/deleteSquad", apiHandling)

// Emergency API
app.get("/api/getEmergencies", apiHandling)

app.get("/api/getEmergencyBySteamID", apiHandling)

app.get("/api/getEmergencyIDBySteamID", apiHandling)

app.post("/api/addEmergency", apiHandling)

app.post("/api/deleteEmergency", apiHandling)

app.post("/api/saveEmergency", apiHandling)

app.post("/api/addEmployeeToEmergency", apiHandling)

app.post("/api/removeEmployeeFromEmergency", apiHandling)

app.post("/api/addMultipleEmployeesToEmergency", apiHandling)

/* app.get("/api/selftest", function(req, res) {
	if (selfTestHandler(req, res)) return
	let moduleName = "API.selftest"
	let clientIP = req.headers['x-forwarded-for'] || req.connection.remoteAddress
	if (expressConfig.adminWhitelist.indexOf(clientIP) != -1) {
		res.render(path.join(__dirname + "/assets/ejs/selftest.ejs"), {active: selfTestActive})
	} else {
		error_page_handling(403, res)
	}
})

app.get("/api/prepareForSelfTest", function(req, res) {
	if (selfTestHandler(req, res)) return
	let moduleName = "API.prepareForSelfTest"
	let retVal = {
		"success": null,
		"data": null,
		"error": null
	}
	let clientIP = req.headers['x-forwarded-for'] || req.connection.remoteAddress
	if (expressConfig.adminWhitelist.indexOf(clientIP) != -1) {
		selfTestActive = true
		if (fs.existsSync("./data_selftest.bin")) fs.unlinkSync("./data_selftest.bin")
		dataBin = new DataManager("./data_selftest.bin")
		retVal.success = true
		res.statusCode = 200
		res.setHeader('Content-Type', 'application/json')
		res.end(JSON.stringify(retVal))
	} else {
		error_page_handling(403, res)
	}
})

app.get("/api/selfTestFinished", function(req, res) {
	if (selfTestHandler(req, res)) return
	let moduleName = "API.selfTestFinished"
	let retVal = {
		"success": null,
		"data": null,
		"error": null
	}
	let clientIP = req.headers['x-forwarded-for'] || req.connection.remoteAddress
	if (expressConfig.adminWhitelist.indexOf(clientIP) != -1) {
		dataBin = new DataManager(dataSaveLocation)
		selfTestActive = false
		retVal.success = true
		res.statusCode = 200
		res.setHeader('Content-Type', 'application/json')
		res.end(JSON.stringify(retVal))
	} else {
		error_page_handling(403, res)
	}
}) */

app.get("/error", function(req, res) {
	if (selfTestHandler(req, res)) return
	error_page_handling(req.query["status"], res)
})

app.get("*", function(req, res) {
	if (selfTestHandler(req, res)) return
	error_page_handling(404, res)
})

app.use(function (err, req, res, next) {
	debugHand.error(err.message, moduleName, err.stack)
	error_page_handling(500, res)
})

process.on('uncaughtException', function(err) {
	debugHand.error(err.message, moduleName, err.stack)
})

process.on('unhandledRejection', function(reason, p){
	debugHand.error(reason, moduleName, p)
})


// Code execution
const server = app.listen(expressConfig.port, expressConfig.host)
debugHand.log(`HTTP server running on ${expressConfig.host}:${expressConfig.port}. Waiting for requests...`, 0, "HTTP-Server")
